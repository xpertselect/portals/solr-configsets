# XpertSelect / Portals / Solr configsets

[gitlab.com/xpertselect/portals/solr-configsets](https://gitlab.com/xpertselect/portals/solr-configsets)

This repository contains the [Apache Solr](https://solr.apache.org) configsets used in the XpertSelect Portals stack.

## License

View the `LICENSE.md` file for licensing details.

## Docker image

The Apache Solr configsets are made available as a light-weight [Docker](https://www.docker.com) image in the [Gitlab.com](https://gitlab.com) container registry via `registry.gitlab.com/xpertselect/portals/solr-configsets`. This image contains _only_ the configsets contained in this repository! The below example illustrates how this image can be incorporated into an [Apache Solr Docker image](https://hub.docker.com/_/solr).

```Dockerfile
FROM solr:8.11

COPY --chown=8983:8983 --from=registry.gitlab.com/xpertselect/portals/solr-configsets:latest \
    /solr-configsets/portals_ckan \
    /opt/solr/server/solr/configsets/portals_ckan
```

A new version of the image is made available for each release. Additionally, a `latest` tag is available to access the latest working version.

## Composer

The Apache Solr configsets are made available as a [Composer](https://getcomposer.org) package under the name [`xpertselect-portals/solr-configsets`](https://packagist.org/packages/xpertselect-portals/solr-configsets) on [Packagist.org](https://packagist.org).

```shell
composer require xpertselect-portals/solr-configsets
```

A new version of the package is made available for each release.
