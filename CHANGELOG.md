# Changelog

## Unreleased (`dev-main`)

### Added

### Changed

### Fixed

---

## Release 1.7.0

### Added

- New fields for WOO information category are added: `information_category` and `facet_information_category`.

---

## Release 1.6.0

### Changed

- The synonym file for simple hierarchy support for the document type facet field has been removed.

---

## Release 1.5.3

### Added

- New fields for WOO document type are added: `document_type` and `facet_document_type`. The synonym file `lang/hierarchy_document_type.txt` is active on the facet field.

---

## Release 1.5.2

### Added

- The `highlight` search component with a `html` encoder.

### Changed

- The `title` field is now explicitly defined as a search field.
- The `title` search field is boosted by a factor of 1.3.

---

## Release 1.5.1

### Changed

- The `description` field of the `portals_register` configset is not of type string anymore. This caused errors for larger values.

---

## Release 1.5.0

### Added

- A new `portals_register` configset, which will be suitable for multiple document types.

---

## Release 1.4.0

### Added

- The `portals_ckan` config set now has a new `/mlt` endpoint for Solr's More Like This. 

---

## Release 1.3.0

### Added

- Add a new field called `notes_stripped` that contains a HTML stripped version of `notes`. The original notes are preserved in the `notes` field.

---

## Release 1.2.0

### Changed

- Renamed the `/suggest_tags` endpoint to the more generic `/autocomplete` in the `portals_ckan` configset. Callers must now explicitly declare the `suggest.dictionary` argument themselves.

---

## Release 1.1.0

### Added

- Add the `/suggest_tags` endpoint to get tag suggestions in the `portals_ckan` configset.
