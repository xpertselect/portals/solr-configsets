FROM alpine:latest AS build

COPY ./ /solr-configsets

RUN /solr-configsets/bin/generate-metadata.sh

FROM scratch

COPY --from=build \
    /solr-configsets/configsets \
    /solr-configsets
