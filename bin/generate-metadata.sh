#!/bin/sh

set -eu

PROJECT_ROOT="$(realpath "$(dirname "$0")/../")"
cd "${PROJECT_ROOT}" || exit 1

echo "Generating configset metadata"
for d in ./configsets/*/; do
    CONFIGSET=$(basename "$(realpath "$d")")
    METADATA_PATH="./configsets/${CONFIGSET}/conf/metadata.txt"

    if [ -f "${METADATA_PATH}" ]; then
        rm -r "${METADATA_PATH}"
    fi

    echo "> Adding metadata to configset ${CONFIGSET}"
    cat > "${METADATA_PATH}" <<EOF
# Configset: ${CONFIGSET}
# Generated on: $(date)
# Source: https://gitlab.com/xpertselect/portals/solr-configsets
EOF
done
