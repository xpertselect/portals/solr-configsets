<?xml version="1.0" encoding="UTF-8" ?>
<!--
 MIT License

 Copyright (c) 2023 Textinfo B.V.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
-->
<schema name="ckan" version="2.8">
  <types>
    <fieldType name="string" class="solr.StrField" sortMissingLast="true" omitNorms="true"/>
    <fieldType name="boolean" class="solr.BoolField" sortMissingLast="true" omitNorms="true"/>
    <fieldType name="int" class="solr.IntPointField" docValues="true"/>
    <fieldType name="float" class="solr.FloatPointField" docValues="true"/>
    <fieldType name="date" class="solr.DatePointField" docValues="true"/>

    <fieldType name="text" class="solr.TextField" positionIncrementGap="100">
      <analyzer>
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.WordDelimiterGraphFilterFactory" generateWordParts="1"
                generateNumberParts="1" catenateWords="1" catenateNumbers="1"
                catenateAll="0" splitOnCaseChange="1"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.StopFilterFactory" ignoreCase="true" words="lang/stopwords_nl.txt"/>
        <filter class="solr.SnowballPorterFilterFactory" language="Dutch"/>
        <filter class="solr.ASCIIFoldingFilterFactory"/>
      </analyzer>
    </fieldType>

    <fieldType class="solr.TextField" name="text_basic" positionIncrementGap="100">
      <analyzer>
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
      </analyzer>
    </fieldType>

    <fieldType name="NGram" class="solr.TextField" positionIncrementGap="100">
      <analyzer type="index">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.WordDelimiterGraphFilterFactory" generateWordParts="1"
                generateNumberParts="1" catenateWords="1" catenateNumbers="1"
                catenateAll="0" splitOnCaseChange="1"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.StopFilterFactory" ignoreCase="true" words="lang/stopwords_nl.txt"/>
        <filter class="solr.SnowballPorterFilterFactory" language="Dutch"/>
        <filter class="solr.NGramFilterFactory" minGramSize="2" maxGramSize="25"/>
        <filter class="solr.ASCIIFoldingFilterFactory"/>
      </analyzer>
      <analyzer type="query">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.WordDelimiterGraphFilterFactory" generateWordParts="1"
                generateNumberParts="1" catenateWords="1" catenateNumbers="1"
                catenateAll="0" splitOnCaseChange="1"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
        <filter class="solr.StopFilterFactory" ignoreCase="true" words="lang/stopwords_nl.txt"/>
        <filter class="solr.SnowballPorterFilterFactory" language="Dutch"/>
        <filter class="solr.ASCIIFoldingFilterFactory"/>
      </analyzer>
    </fieldType>

    <fieldType name="suggestion_stemmed" class="solr.TextField" positionIncrementGap="100">
      <analyzer type="index">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.WordDelimiterGraphFilterFactory" generateWordParts="1"
                generateNumberParts="1" catenateWords="1" catenateNumbers="1"
                catenateAll="0" splitOnCaseChange="1"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.SnowballPorterFilterFactory" language="Dutch"/>
        <filter class="solr.NGramFilterFactory" minGramSize="2" maxGramSize="25"/>
        <filter class="solr.ASCIIFoldingFilterFactory"/>
      </analyzer>
      <analyzer type="query">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.WordDelimiterGraphFilterFactory" generateWordParts="1"
                generateNumberParts="1" catenateWords="1" catenateNumbers="1"
                catenateAll="0" splitOnCaseChange="1"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
        <filter class="solr.SnowballPorterFilterFactory" language="Dutch"/>
        <filter class="solr.ASCIIFoldingFilterFactory"/>
      </analyzer>
    </fieldType>

    <fieldType name="suggestion" class="solr.TextField" positionIncrementGap="100">
      <analyzer type="index">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.WordDelimiterGraphFilterFactory" generateWordParts="1"
                generateNumberParts="1" catenateWords="1" catenateNumbers="1"
                catenateAll="0" splitOnCaseChange="1"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.NGramFilterFactory" minGramSize="2" maxGramSize="25"/>
        <filter class="solr.ASCIIFoldingFilterFactory"/>
      </analyzer>
      <analyzer type="query">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.WordDelimiterGraphFilterFactory" generateWordParts="1"
                generateNumberParts="1" catenateWords="1" catenateNumbers="1"
                catenateAll="0" splitOnCaseChange="1"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
        <filter class="solr.ASCIIFoldingFilterFactory"/>
      </analyzer>
    </fieldType>

    <fieldType name="hierarchy_theme" class="solr.TextField" positionIncrementGap="100">
      <analyzer>
        <tokenizer class="solr.KeywordTokenizerFactory"/>
        <filter class="solr.SynonymGraphFilterFactory" synonyms="lang/hierarchy_theme.txt"
                ignoreCase="false" expand="true"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
      </analyzer>
    </fieldType>

    <fieldType name="spellcheck" class="solr.TextField" positionIncrementGap="100"
               multiValued="true">
      <analyzer>
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.StopFilterFactory" ignoreCase="true" words="lang/stopwords_nl.txt"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.ASCIIFoldingFilterFactory"/>
      </analyzer>
    </fieldType>
  </types>

  <fields>
    <!-- Solr system fields -->
    <field name="_version_" type="string" indexed="true" stored="true" multiValued="false"/>

    <!-- DCAT Dataset -->
    <field name="identifier" type="string" indexed="true" stored="true" multiValued="false"/>
    <field name="alternate_identifier" type="string" indexed="true" stored="true"
           multiValued="true"/>
    <field name="language" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="authority" type="string" indexed="true" stored="true" multiValued="false"/>
    <field name="contact_point_email" type="string" indexed="false" stored="false"
           multiValued="false"/>
    <field name="contact_point_name" type="string" indexed="false" stored="false"
           multiValued="false"/>
    <field name="contact_point_phone" type="string" indexed="false" stored="false"
           multiValued="false"/>
    <field name="contact_point_website" type="string" indexed="false" stored="false"
           multiValued="false"/>
    <field name="access_rights" type="string" indexed="true" stored="true" multiValued="false"/>
    <field name="conforms_to" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="related_resource" type="string" indexed="true" stored="true"
           multiValued="true"/>
    <field name="source" type="string" indexed="false" stored="false" multiValued="true"/>
    <field name="version" type="string" indexed="false" stored="false" multiValued="false"/>
    <field name="version_notes" type="string" indexed="false" stored="false"
           multiValued="true"/>
    <field name="issued" type="date" indexed="false" stored="false" multiValued="false"/>
    <field name="has_version" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="is_version_of" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="legal_foundation_label" type="text" indexed="true" stored="true"
           multiValued="false"/>
    <field name="legal_foundation_ref" type="string" indexed="false" stored="false"
           multiValued="false"/>
    <field name="legal_foundation_uri" type="string" indexed="false" stored="false"
           multiValued="false"/>
    <field name="frequency" type="string" indexed="true" stored="true" multiValued="false"/>
    <field name="provenance" type="string" indexed="false" stored="false" multiValued="true"/>
    <field name="documentation" type="string" indexed="false" stored="false"
           multiValued="true"/>
    <field name="sample" type="string" indexed="false" stored="false" multiValued="true"/>
    <field name="metadata_language" type="string" indexed="true" stored="true"
           multiValued="false"/>
    <field name="theme" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="source_catalog" type="string" indexed="true" stored="true"
           multiValued="false"/>
    <field name="changetype" type="string" indexed="false" stored="false" multiValued="false"/>
    <field name="modified" type="date" indexed="false" stored="false" multiValued="false"/>
    <field name="spatial_scheme" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="spatial_value" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="temporal_label" type="text" indexed="true" stored="true" multiValued="false"/>
    <field name="temporal_end" type="date" indexed="true" stored="false" multiValued="false"/>
    <field name="temporal_start" type="date" indexed="true" stored="false" multiValued="false"/>
    <field name="dataset_status" type="string" indexed="true" stored="true"
           multiValued="false"/>
    <field name="date_planned" type="date" indexed="true" stored="true" multiValued="false"/>
    <field name="high_value" type="boolean" indexed="true" stored="true"/>
    <field name="referentie_data" type="boolean" indexed="true" stored="true"/>
    <field name="basis_register" type="boolean" indexed="true" stored="true"/>

    <!-- DCAT Distribution -->
    <field name="res_metadata_language" type="string" indexed="true" stored="true"
           multiValued="true"/>
    <field name="res_language" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="res_license_id" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="res_size" type="int" indexed="false" stored="false" multiValued="true"/>
    <field name="res_download_url" type="string" indexed="true" stored="true"
           multiValued="true"/>
    <field name="res_mimetype" type="string" indexed="true" stored="false" multiValued="true"/>
    <field name="res_release_date" type="date" indexed="true" stored="true" multiValued="true"/>
    <field name="res_rights" type="string" indexed="false" stored="false" multiValued="true"/>
    <field name="res_status" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="res_modification_date" type="date" indexed="true" stored="true"
           multiValued="true"/>
    <field name="res_linked_schemas" type="string" indexed="false" stored="false"
           multiValued="true"/>
    <field name="res_hash" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="res_hash_algorithm" type="string" indexed="true" stored="true"
           multiValued="true"/>
    <field name="res_documentation" type="string" indexed="false" stored="false"
           multiValued="true"/>
    <field name="res_distribution_type" type="string" indexed="true" stored="true"
           multiValued="true"/>

    <!-- CKAN Package -->
    <field name="index_id" type="string" indexed="true" stored="true" required="true"/>
    <field name="id" type="string" indexed="true" stored="true" required="true"/>
    <field name="site_id" type="string" indexed="true" stored="true" required="true"/>
    <field name="title" type="text" indexed="true" stored="true" multiValued="false"/>
    <field name="entity_type" type="string" indexed="true" stored="true" omitNorms="true"/>
    <field name="dataset_type" type="string" indexed="true" stored="true"/>
    <field name="state" type="string" indexed="true" stored="true" omitNorms="true"/>
    <field name="name" type="text" indexed="true" stored="true" multiValued="false"/>
    <field name="revision_id" type="string" indexed="true" stored="true" omitNorms="true"/>
    <field name="url" type="string" indexed="true" stored="true" multiValued="false"/>
    <field name="ckan_url" type="string" indexed="true" stored="true" omitNorms="true"/>
    <field name="download_url" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="notes" type="text" indexed="true" stored="true" multiValued="false"/>
    <field name="notes_stripped" type="text" indexed="true" stored="true" multiValued="false"/>
    <field name="author" type="text" indexed="true" stored="true"/>
    <field name="author_email" type="text" indexed="true" stored="true"/>
    <field name="maintainer" type="text" indexed="true" stored="true"/>
    <field name="maintainer_email" type="text" indexed="true" stored="true"/>
    <field name="license" type="string" indexed="true" stored="true" multiValued="false"/>
    <field name="license_id" type="string" indexed="true" stored="true"/>
    <field name="ratings_average" type="float" indexed="true" stored="false"/>
    <field name="ratings_count" type="int" indexed="true" stored="false"/>
    <field name="tags" type="text" indexed="true" stored="true" multiValued="true"/>
    <field name="groups" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="organization" type="string" indexed="true" stored="true" multiValued="false"/>
    <field name="capacity" type="string" indexed="true" stored="true" multiValued="false"/>
    <field name="permission_labels" type="string" indexed="true" stored="false"
           multiValued="true"/>

    <!-- CKAN Resource -->
    <field name="res_description" type="text" indexed="true" stored="true"
           multiValued="true"/>
    <field name="res_format" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="res_name" type="text" indexed="true" stored="true" multiValued="true"/>
    <field name="res_type" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="res_url" type="string" indexed="true" stored="true" multiValued="true"/>

    <!-- CKAN Misq -->
    <field name="urls" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="depends_on" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="dependency_of" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="derives_from" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="has_derivation" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="links_to" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="linked_from" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="child_of" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="parent_of" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="views_total" type="int" indexed="true" stored="false"/>
    <field name="views_recent" type="int" indexed="true" stored="false"/>
    <field name="resources_accessed_total" type="int" indexed="true" stored="false"/>
    <field name="resources_accessed_recent" type="int" indexed="true" stored="false"/>
    <field name="metadata_created" type="date" indexed="true" stored="true"/>
    <field name="metadata_modified" type="date" indexed="true" stored="true"/>
    <field name="indexed_ts" type="date" indexed="true" stored="true" default="NOW"/>
    <field name="data_dict" type="string" indexed="false" stored="true"/>
    <field name="validated_data_dict" type="string" indexed="false" stored="true"/>
    <field name="title_string" type="string" indexed="true" stored="false"/>

    <!-- Search fields -->
    <field name="text" type="text" indexed="true" stored="false" multiValued="true"/>
    <field name="ngram_text" type="NGram" indexed="true" stored="false" multiValued="true"/>

    <!-- Search suggestion fields -->
    <field name="suggestion" type="suggestion" indexed="true" stored="true"/>
    <field name="suggestion_stemmed" type="suggestion_stemmed" indexed="true" stored="true"/>

    <!-- Spellcheck field -->
    <field name="spellcheck" type="spellcheck" indexed="true" stored="true" multiValued="true"/>

    <!-- Tag suggestion field-->
    <field name="tags_suggestion" type="text_basic" indexed="true" stored="true" multiValued="true"/>

    <!--  Search Facets  -->
    <field name="facet_access_rights" type="string" indexed="true" stored="true"
           multiValued="false"/>
    <field name="facet_dataset_status" type="string" indexed="true" stored="true"
           multiValued="false"/>
    <field name="facet_license_id" type="string" indexed="true" stored="true"/>
    <field name="facet_res_format" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="facet_tags" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="facet_theme" type="hierarchy_theme" indexed="true" stored="true"
           multiValued="true"/>
    <field name="facet_res_distribution_type" type="string" indexed="true" stored="true" multiValued="true"/>
    <field name="facet_organization" type="string" indexed="true" stored="true" multiValued="false"/>

    <!-- Dynamic -->
    <dynamicField name="*_date" type="date" indexed="true" stored="true"/>
    <dynamicField name="extras_*" type="text" indexed="true" stored="true"/>
    <dynamicField name="res_extras_*" type="text" indexed="true" stored="true"
                  multiValued="true"/>
    <dynamicField name="vocab_*" type="string" indexed="true" stored="true" multiValued="true"/>
    <dynamicField name="*" type="string" indexed="true" stored="false"/>
  </fields>

  <uniqueKey>index_id</uniqueKey>
  <solrQueryParser/>

  <copyField source="author" dest="text"/>
  <copyField source="authority" dest="text"/>
  <copyField source="ckan_url" dest="urls"/>
  <copyField source="conforms_to" dest="text"/>
  <copyField source="description" dest="text"/>
  <copyField source="download_url" dest="urls"/>
  <copyField source="extras_*" dest="text"/>
  <copyField source="groups" dest="text"/>
  <copyField source="license_id" dest="text"/>
  <copyField source="maintainer" dest="text"/>
  <copyField source="name" dest="text"/>
  <copyField source="notes_stripped" dest="text"/>
  <copyField source="organization" dest="text"/>
  <copyField source="publisher" dest="text"/>
  <copyField source="related_resource" dest="text"/>
  <copyField source="res_description" dest="text"/>
  <copyField source="res_extras_*" dest="text"/>
  <copyField source="res_name" dest="text"/>
  <copyField source="res_url" dest="urls"/>
  <copyField source="tags" dest="text"/>
  <copyField source="text" dest="text"/>
  <copyField source="theme" dest="text"/>
  <copyField source="title" dest="ngram_text"/>
  <copyField source="organization" dest="ngram_text"/>
  <copyField source="title_string" dest="suggestion"/>
  <copyField source="title_string" dest="suggestion_stemmed"/>
  <copyField source="url" dest="urls"/>
  <copyField source="urls" dest="text"/>
  <copyField source="vocab_*" dest="text"/>

  <!-- Facet copy fields -->
  <copyField source="access_rights" dest="facet_access_rights"/>
  <copyField source="dataset_status" dest="facet_dataset_status"/>
  <copyField source="license_id" dest="facet_license_id"/>
  <copyField source="res_format" dest="facet_res_format"/>
  <copyField source="tags" dest="facet_tags"/>
  <copyField source="theme" dest="facet_theme"/>
  <copyField source="res_distribution_type" dest="facet_res_distribution_type"/>
  <copyField source="organization" dest="facet_organization"/>

  <!-- Copy to spellcheck field -->
  <copyField source="title_string" dest="spellcheck"/>
  <copyField source="notes_stripped" dest="spellcheck"/>
  <copyField source="tags" dest="spellcheck"/>

  <!-- Copy to tag suggestion field -->
  <copyField source="tags" dest="tags_suggestion"/>
</schema>
